/*
Copyright © 2020 Agus Purnomo <agus.purnomo@bluebirdgroup.com>

*/
package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var cfgFile string

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "xprops",
	Short: "A brief description of your application",
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 0 {
			return
		}

		//for i, key := range viper.AllKeys() {
		//	fmt.Println(i, key)
		//}
		fmt.Println(viper.GetString(args[0]))
	},
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	rootCmd.PersistentFlags().StringVarP(&cfgFile, "file", "f", "",
		"Configuration file")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		viper.SetConfigFile(cfgFile)
	}

	viper.AutomaticEnv() // read in environment variables that match

	if cfgFile != "" {
		if err := viper.ReadInConfig(); err != nil {
			fmt.Fprintln(os.Stderr, "Unable to read configuration file:", viper.ConfigFileUsed())
			os.Exit(1)
		}
	}
}
