/*
Copyright © 2020 Agus Purnomo <agus.purnomo@bluebirdgroup.com>

*/
package main

import "gitlab.com/watonist/xprops/cmd"

func main() {
	cmd.Execute()
}
